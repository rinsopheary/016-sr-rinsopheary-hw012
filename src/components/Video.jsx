import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
} from "react-router-dom";

// Since routes are regular React components, they
// may be rendered anywhere in the app, including in
// child elements.
//
// This helps when it's time to code-split your app
// into multiple bundles because code-splitting a
// React Router app is the same as code-splitting
// any other React app.

export default function Video() {
  return (
    <div className="container pt-5">
      <Router>
        <div>
          <ul>
            <li>
              <Link to="/animation">Animation</Link>
            </li>
            <li>
              <Link to="/movies">Movies</Link>
            </li>
          </ul>

          <Switch>
            <Route exact path="/video">
              <h3>Please select a topic.</h3>
            </Route>
            <Route path="/animation">
              <Animation />
            </Route>
            <Route path="/movies">
              <Movies />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

function Animation() {
  // The `path` lets us build <Route> paths that are
  // relative to the parent route, while the `url` lets
  // us build relative links.
  let { path, url } = useRouteMatch();

  return (
    <div>
      <h2>Animation Catagories</h2>
      <ul>
        <li>
          <Link to={`${url}/action`}>Action</Link>
        </li>
        <li>
          <Link to={`${url}/romance`}>Romance</Link>
        </li>
        <li>
          <Link to={`${url}/comedy`}>Comedy</Link>
        </li>
      </ul>

      <Switch>
        <Route exact path={path}>
          <h3>Please select a topic.</h3>
        </Route>
        <Route path={`${path}/:topicId`}>
          <Topic />
        </Route>
      </Switch>
    </div>
  );
}

function Movies() {
  // The `path` lets us build <Route> paths that are
  // relative to the parent route, while the `url` lets
  // us build relative links.
  let { path, url } = useRouteMatch();

  return (
    <div>
      <h2>Movie catagories</h2>
      <ul>
        <li>
          <Link to={`${url}/advanture`}>Advanture</Link>
        </li>
        <li>
          <Link to={`${url}/comedy`}>Comedy</Link>
        </li>
        <li>
          <Link to={`${url}/crime`}>Crime</Link>
        </li>
        <li>
          <Link to={`${url}/documentary`}>Documentary</Link>
        </li>
      </ul>

      <Switch>
        <Route exact path={path}>
          <h3>Please select a topic.</h3>
        </Route>
        <Route path={`${path}/:topicId`}>
          <Topic />
        </Route>
      </Switch>
    </div>
  );
}

function Topic() {
  // The <Route> that rendered this component has a
  // path of `/topics/:topicId`. The `:topicId` portion
  // of the URL indicates a placeholder that we can
  // get from `useParams()`.
  let { topicId } = useParams();

  return (
    <div>
      <h3>{topicId}</h3>
    </div>
  );
}
