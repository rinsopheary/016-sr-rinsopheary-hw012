import React from "react";

export default function Posts(props) {
  let post = props.match.params.id; //get id from url
  return (
    <div className="container pt-5">
      <p>This is content from post {post}</p>
    </div>
  );
}
