import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Auth(props) {
  return (
    <div className="container pt-5">
      <Form>
        <Form.Group controlId="">
          <Form.Label>Username:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Username"
            name="username"
            onChange={props.onChange}
          />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter Password"
            name="password"
            onChange={props.onChange}
          />
        </Form.Group>
        <Link to="/welcome">
          <Button variant="primary" type="submit" onClick={props.onSubmit}>
            Submit
          </Button>
        </Link>
      </Form>
    </div>
  );
}
