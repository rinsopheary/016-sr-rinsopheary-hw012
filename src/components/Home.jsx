import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import CardItem from "./CardItem";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [
        {
          id: 1,
          title: "Ben 10 history",
          description:
            "Ben 10 is an American franchise and television series created by Man of Action Studios and produced by Cartoon Network Studios. The franchise revolves around a boy named Ben Tennyson who acquires a watch-style alien device, the Omnitrix, which allows him to transform into ten (plus more) different alien creatures",
          image:
            "https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/boxart_images/3265031c-4379-4e71-b5d0-c1c6fd3ed90a-fc00f9dd-98e1-481b-ab77-2a973d48d7bc_RGB_SD._RI_VQjecRzAwbarZURpTz4aZHZIIoOBcsV_TTW_.jpg",
        },
        {
          id: 2,
          title: "Ben 10 history",
          description:
            "Ben 10 is an American franchise and television series created by Man of Action Studios and produced by Cartoon Network Studios. The franchise revolves around a boy named Ben Tennyson who acquires a watch-style alien device, the Omnitrix, which allows him to transform into ten (plus more) different alien creatures",
          image:
            "https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/boxart_images/3265031c-4379-4e71-b5d0-c1c6fd3ed90a-fc00f9dd-98e1-481b-ab77-2a973d48d7bc_RGB_SD._RI_VQjecRzAwbarZURpTz4aZHZIIoOBcsV_TTW_.jpg",
        },
        {
          id: 3,
          title: "Ben 10 history",
          description:
            "Ben 10 is an American franchise and television series created by Man of Action Studios and produced by Cartoon Network Studios. The franchise revolves around a boy named Ben Tennyson who acquires a watch-style alien device, the Omnitrix, which allows him to transform into ten (plus more) different alien creatures",
          image:
            "https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/boxart_images/3265031c-4379-4e71-b5d0-c1c6fd3ed90a-fc00f9dd-98e1-481b-ab77-2a973d48d7bc_RGB_SD._RI_VQjecRzAwbarZURpTz4aZHZIIoOBcsV_TTW_.jpg",
        },
        {
          id: 4,
          title: "Ben 10 history",
          description:
            "Ben 10 is an American franchise and television series created by Man of Action Studios and produced by Cartoon Network Studios. The franchise revolves around a boy named Ben Tennyson who acquires a watch-style alien device, the Omnitrix, which allows him to transform into ten (plus more) different alien creatures",
          image:
            "https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/boxart_images/3265031c-4379-4e71-b5d0-c1c6fd3ed90a-fc00f9dd-98e1-481b-ab77-2a973d48d7bc_RGB_SD._RI_VQjecRzAwbarZURpTz4aZHZIIoOBcsV_TTW_.jpg",
        },
        {
          id: 5,
          title: "Ben 10 history",
          description:
            "Ben 10 is an American franchise and television series created by Man of Action Studios and produced by Cartoon Network Studios. The franchise revolves around a boy named Ben Tennyson who acquires a watch-style alien device, the Omnitrix, which allows him to transform into ten (plus more) different alien creatures",
          image:
            "https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/boxart_images/3265031c-4379-4e71-b5d0-c1c6fd3ed90a-fc00f9dd-98e1-481b-ab77-2a973d48d7bc_RGB_SD._RI_VQjecRzAwbarZURpTz4aZHZIIoOBcsV_TTW_.jpg",
        },
        {
          id: 6,
          title: "Ben 10 history",
          description:
            "Ben 10 is an American franchise and television series created by Man of Action Studios and produced by Cartoon Network Studios. The franchise revolves around a boy named Ben Tennyson who acquires a watch-style alien device, the Omnitrix, which allows him to transform into ten (plus more) different alien creatures",
          image:
            "https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/boxart_images/3265031c-4379-4e71-b5d0-c1c6fd3ed90a-fc00f9dd-98e1-481b-ab77-2a973d48d7bc_RGB_SD._RI_VQjecRzAwbarZURpTz4aZHZIIoOBcsV_TTW_.jpg",
        },
      ],
    };
  }

  render() {
    let allData = this.state.items.map((d) => <CardItem data={d} />);
    return (
      <div className="container pb-5">
        <div className="row">{allData}</div>
      </div>
    );
  }
}
