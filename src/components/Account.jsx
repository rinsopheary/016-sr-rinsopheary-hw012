import React from "react";
import queryString from "query-string";
import { Link } from "react-router-dom";

export default function Account(props) {
  let pv = queryString.parse(props.location.search);
  console.log(pv.name);

  return (
    <div className="container pt-5">
      <h3>Accounts</h3>
      <li>
        <Link to="/account/?name=netflix">Netflix</Link>
      </li>
      <li>
        <Link to="/account/?name=zillowGroup">Zillow Group</Link>
      </li>
      <li>
        <Link to="/account/?name=yahoo">Yahoo</Link>
      </li>
      <li>
        <Link to="/account/?name=modusCreate">Modus Create</Link>
      </li>
      {pv.name === undefined ? (
        <h3>There is no name in the query string</h3>
      ) : (
        <h3>
          The <span className="text-danger">name</span> in the query string is "
          {pv.name}"
        </h3>
      )}
    </div>
  );
}
