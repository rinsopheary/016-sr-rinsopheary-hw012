import React from "react";
import { Redirect } from "react-router-dom";

export default function Welcome(props) {
  console.log(props);

  return props.auth === true ? (
    <h1 className="container pt-5 text-success">Welcome</h1>
  ) : (
    <Redirect to="/auth" />
  );
}
