import React, { Component } from 'react'
import './App.css';
import Menu from './components/Menu';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from './components/Home'
import Main from './components/Main';
import Video from './components/Video';
import Account from './components/Account';
import Auth from './components/Auth';
import Posts from './components/Posts';
import Welcome from './components/Welcome'


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      isAuth: false,
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = () => {
    let { username, password } = this.state;
    let check;
    if (username !== "" && password !== "") {
      check = true;
    } else {
      check = false;
      alert("Please input all field");
    }

    this.setState({
      isAuth: check,
      username: "",
      password: "",
    });
  };

  render() {
    return (
      <div>
        <Router>
          <Menu />
          <Switch>
            <Route path="/" exact component={Main} />
            <Route path="/home" component={Home} />
            <Route path="/video" component={Video} />
            <Route path="/account" component={Account} />
            <Route path="/posts/:id" component={Posts} />
            <Route
                path="/auth"
                render={() => (
                  <Auth
                    onSubmit={this.handleSubmit.bind(this)}
                    onChange={this.handleChange}
                  />
                )}
              />
              <Route
                path="/welcome"
                render={() => <Welcome auth={this.state.isAuth} />}
              />
          </Switch>
        </Router>
      </div>
    )
  }
}

